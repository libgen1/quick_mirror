# A quick way to setup a libgen torrent mirror
### Author: Kallax

So here is a script to make a quick LibGen mirror. Because it downloads while it uploads the first time and then saves it, it doesn't have to wait to first get some gigabyte of books. It is enough to run it and it will be working instantly, even if the first download of each book is slow. It has no database or anything.

But, it is not safe. Because there are no seeders on I2P, it has to use unprotected torrent. This means anyone can find out the server's IP. But if you're using onion, they can only send a DMCA for the torrent. In the future, I hope there will be more seeders on I2P. Until then, it should be possible to use web seed, however this will be slow and cause load on libgen servers. It would also be possible to create a kind of proxy for BitTorrent and run this together with the onion reverse proxies. But let's take one problem at a time gentlemen.

I will assume your server is with Debian, but it should be the same for Ubuntu. You might have to change /usr/sbin/lighttpd below. I don't know.

Setup:
```bash
#!/bin/bash
sudo apt-get install btfs lighttpd
mkdir libgen
mkdir libgen/mount
mkdir libgen/home
echo -e 'server.document-root = "./mount" \n server.port = '"$((49152+($RANDOM*3)%(65535-49152)))"' \n mimetype.assign = (    ".txt" => "text/plain" )' > libgen/lighttpd.conf # port won't overlap and doesn't need root
grep 'server.port =' libgen/lighttpd.conf # to get what port

for i in 0 1 2 294 2442 2443 2444; do # write which IDs you want to mirror
  torsocks -i wget -P libgen/ 'http://gen.lib.rus.ec/repository_torrent/r_'"$i"'000.torrent'
  mkdir 'libgen/mount/r_'"$([ $i == 0 ] || echo -n $i)"'000' #because it can only have one torrent for each folder
done
ls libgen/mount | sort | grep '^r_' > libgen/mount/file_list.txt
# in future maybe libgen onion can support torrent downloads too
```

Run:
```bash
#!/bin/bash
cd libgen
for i in *.torrent; do
  HOME=./home btfs -k "$i" 'mount/r_'"$(echo "$i" | tr -dc 0-9)"
done
/usr/sbin/lighttpd -D -f lighttpd.conf
for i in mount/*; do
  fusermount -u "$i"
done
cd ..
```

So, you run setup first, then install every time after that you want to start it.
To clean up, simply remove the "libgen" folder. It is 100% self contained.
To visit your new mirror, look in the log for the line "server.document-root = XXXXX". Then go to localhost:XXXXX/file_list.txt. Then, to find a book with hash YYYYYYYY and ID ZZZZZZZ, go to http://localhost:XXXXX/r_ZZZZ000/ZZZZ000/YYYYYYYY. For example, http://localhost:12345/r_294000/294000/ ... ca959358cf.
If you want to make a tor onion service, then follow the guide at https://2019.www.torproject.org/docs/to ... tml.en#two. Replace 8080 with the port you got above. For step 4, it should be sufficient to change lighttpd.conf. You want to add this line: 
```bash
server.bind = "127.0.0.1"
``` 
to 
```bash
for i in {0..2500}; do # write which IDs you want to mirror
```

Note that if you're getting this many torrents, it is better to use a "smart" download utility like aria2c, and perhaps a script to find out which ones are up yet.

Good luck!